from flask_restful import Resource
from flask import render_template, make_response
import os
import csv

class HomeHandler(Resource):
    def get(self):
        try:
            return make_response(render_template('index.html'))
        except Exception as e:
            print(e)


class DashboardHandler(Resource):
    def get(self):
        try:
            full_path = os.path.realpath(__file__)
            working_dir = os.path.dirname(full_path)
            # print(working_dir)
            output_file_dir = working_dir+"/media/output.csv"
            # method 2
            with open(output_file_dir) as file:
                reader = csv.reader(file)
                header = next(reader)
                # print(reader[1])
                all_data =[]
                for row in reader:
                    # print(row)
                    if len(row)==19:
                        all_data.append(row)
                    else:
                        for j in range(len(row),len(header)):
                            row.append("NA")
                        all_data.append(row)
                return make_response(render_template("dashboard.html" , header=header, csv=all_data))
        except Exception as e:
            print(e)
            return str(e)