from celery import Celery

# You need to replace the next values with the appropriate values for your configuration
def make_celery(app):
    celery_app = Celery(app.import_name, backend=BaseConfig.result_backend,
                        broker='pyamqp://guest:guest@rabbit:5672//')
    celery_app.conf.update(
            task_serializer='json',
            accept_content=['json'],  # Ignore other content
            result_serializer='json',
            timezone='Asia/Calcutta',
            enable_utc=False,
            result_persistent=False,
            celery_task_routes=BaseConfig.celery_task_routes,
            task_queues=BaseConfig.task_queues,
            task_default_queue=BaseConfig.task_default_queue,
            worker_hijack_root_logger=False,
            task_ignore_result=True,
            broker_heartbeat=10,
            broker_pool_limit=10000,
            broker_connection_retry=True,
            broker_connection_max_retries=None,
            broker_connection_timeout=120
        )
    celery_app.conf.update(app.config)

    class ContextTask(celery_app.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_app.Task = ContextTask
    return celery_app


class BaseConfig:

    CELERY_TIMEZONE = 'Asia/Calcutta'

    # Result backend changed to amqp to use result expires feature. Not very well aware of other amqp vs rpc benefits.
    result_backend = 'amqp://'
    task_default_queue = 'default'
    celery_task_routes = {
                            'app.start_scrapping_task': {'queue': 'start_scrapping_queue'},
                          }
    task_queues = {
        'default': {
            "exchange": "default",
            "binding_key": "default",
        },
        'start_scrapping_queue':{
            'exchange': 'start_scrapping_queue',
            'routing_key': 'start_scrapping_queue',
        },    
    }