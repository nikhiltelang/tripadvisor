def filter_about(about):
    try:
        row_dict = {}
        string = about.text_content()
        string = string.replace("About","")
        data = string.split("reviews#")

        original = data[0] + "reviews"
        if "Excellent" in original:
            original = original.replace("Excellent"," Excellent ")
        elif "Very good" in original:
            original = original.replace("Very good"," VeryGood ")
        splitted_data = original.split()
        rank_list = ['rank','remark','reviews']
        for i in range(0,3):
            row_dict[rank_list[i]] = splitted_data[i]
            # row.append(splitted_data[i])
    except Exception as e:
        print(e)

    #-------------------------------------------------------------
    try:
        rating_bubble_class = about.locator(".ui_bubble_rating")
        rating_name = ["remark_rank","Location","Cleanliness","Service","Value"]
        
        for i in range(1,5):
            data_temp = rating_bubble_class.nth(i)
            data = data_temp.evaluate("node => node.className")
            original_data = data.replace("ui_bubble_rating bubble_","")
            print("Data : ",original_data)
            row_dict[rating_name[i]] = original_data
        print(row_dict)
        # all_col_list = ['rank','remark','reviews',"remark_rank","Location","Cleanliness","Service","Value"]
        # for i,j in row_dict.items():
        #     print(i)
        return row_dict
    except Exception as e:
        print(e)
    # return original
