from flask import Flask, make_response, render_template , request, redirect, jsonify
from flask_restful import Api, Resource
from app.celery_config import make_celery
import os, shutil
import csv
from app.main import run_srap, scrap_with_beautifulsoap
from helper import Create_url, hotel_com, Expedia

from app.handler import HomeHandler, DashboardHandler
flask_app = Flask(__name__,instance_relative_config=True)

api = Api(flask_app)
celery_obj = make_celery(flask_app)

class ScrapHandler(Resource):
    def post(self):
        try:
            file = request.files['file']
            full_path = os.path.realpath(__file__)
            working_dir = os.path.dirname(full_path)
            
            # check media directory
            check_media_files = working_dir+"/media/"
            if os.listdir(check_media_files):
                shutil.rmtree(check_media_files)
                os.makedirs(check_media_files)

            filename = working_dir+"/media/"+file.filename
            file.save(filename)
            destnation_file = os.path.join(working_dir,'media/output.csv')
            data = {"filename":filename,"destination":destnation_file}
            # run_srap(filename,destnation_file)
            scrap_with_beautifulsoap(filename,destnation_file)
            # start_scrap.delay(data)
            return redirect("/dashboard")
        except Exception as e:
            print(e)
            return str(e)

@celery_obj.task(queue='start_scrapping_queue')
def start_scrap(data):
    filename = data['filename']
    destination = data['destination']
    # run_srap(filename,destination)
    scrap_with_beautifulsoap(filename,destination)


api.add_resource(HomeHandler,'/')
api.add_resource(ScrapHandler,'/csv')
api.add_resource(DashboardHandler,'/dashboard')



# which URL is associated function
@flask_app.route('/scrap/booking', methods =["POST"])
def booking__com():
    if request.method == "POST":
        hotel_name = request.form.get("hotel_name")
        city = request.form.get("city")
        number_of_adults = request.form.get("number_of_adults")
        number_of_children = request.form.get("number_of_children")
        number_of_rooms = request.form.get("number_of_rooms")
        start_date = request.form.get("start_date")
        end_date = request.form.get("end_date")

        scrap_obj = Create_url(hotel_name,city,number_of_adults,number_of_children,number_of_rooms,start_date,end_date)
        url = scrap_obj.create_url()
        data = scrap_obj.start_scrapping_with_selenium(url)
        return jsonify(data)
    return render_template("form.html")

@flask_app.route('/scrap/hotels', methods =["POST"])
def hotel__com():
    if request.method == "POST":
        hotel_name = request.form.get("hotel_name")
        city = request.form.get("city")
        number_of_adults = request.form.get("number_of_adults")
        number_of_children = request.form.get("number_of_children")
        number_of_rooms = request.form.get("number_of_rooms")
        start_date = request.form.get("start_date")
        end_date = request.form.get("end_date")

        obj = hotel_com(hotel_name,city,number_of_adults,number_of_children,number_of_rooms,start_date,end_date)
        url = obj.create_url()

        # print(url)
        data = obj.scrap_with_beautifulsoap(url)
        return jsonify(data)
    return render_template("form.html")


@flask_app.route('/scrap/expedia', methods =["POST"])
def expedia__com():
    if request.method == "POST":
        hotel_name = request.form.get("hotel_name")
        city = request.form.get("city")
        number_of_adults = request.form.get("number_of_adults")
        number_of_children = request.form.get("number_of_children")
        number_of_rooms = request.form.get("number_of_rooms")
        start_date = request.form.get("start_date")
        end_date = request.form.get("end_date")

        obj = Expedia(hotel_name,city,number_of_adults,number_of_children,number_of_rooms,start_date,end_date)
        url = obj.create_url()
        data = obj.scrap_with_beautifulsoap(url)
        return jsonify(data)
    return render_template("form.html")
