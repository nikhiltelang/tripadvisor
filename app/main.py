from playwright.sync_api import sync_playwright, TimeoutError
from time import sleep
import csv
from app.helper import filter_about
# from app import flask_app
from bs4 import BeautifulSoup
import requests
from selenium import webdriver
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def start_scrapping(topic,page, browser):
    # Add topic name in search box
    page.get_by_role("searchbox").last.fill(topic)
    # press Enter key after fill topic
    page.keyboard.press("Enter")
    try:
        page.wait_for_load_state('domcontentloaded')
        # find results
        result = page.locator(".result-title")
        # click on first items in result
        result.first.click()
        # switch 2nd tab
        page2 = page.context.pages[1]
        # fetch all required data in 2nd tab
        about = page2.locator('#ABOUT_TAB').first
        # filter all data 
        rank = filter_about(about)
        # close second tab
        page2.close()
        # return data
        return rank
    except TimeoutError as te:
        print(te)
    except Exception as e:
        print(e)

# @celery_obj.task(queue='start_scrapping_task')
def run_srap(filename,destination):
    with sync_playwright() as p:
        try:
            # launch chromium browser
            browser = p.chromium.launch(headless=False)
            # open new tab
            page = browser.new_page()
            page.set_default_timeout(0)
            # open tripadvisor website
            page.goto("https://www.tripadvisor.in/Search?q=Quinta%20de%20la%20Rosa&searchNearby=false&blockRedirect=true&geo=1&ssrc=h&rf=1")
            page.wait_for_load_state('domcontentloaded')
            # create list of csv file headers
            fields = ['TBOHOTELID','GIATAID','HOTELNAME','ADDRESSLINE1','ADDRESSLINE2','CITYID_NEW','CITYNAME','COUNTRYNAME','COUNTRYCODE','LATITUDE','LONGITUDE','STARRATING','RANK','REMARK','REVIEWS',"LOCATION","CLEANLINESS","SERVICE","VALUE"]
            # open csv file
            with open(filename) as f:
                with open(destination, 'a') as csvfile_1: 
                    # create csv file object
                    csvwriter1 = csv.writer(csvfile_1)
                    # add header in csv file
                    csvwriter1.writerow(fields)
                    csvfile_1.close()
                    csvreader = csv.reader(f)
                    
                    for i,row in enumerate(csvreader):
                        with open(destination, 'a') as csvfile: 
                            # create csv file object
                            csvwriter = csv.writer(csvfile)
                            rows = []
                            # print(row)
                            if row[2] != "HOTELNAME":
                                # Start scrapping
                                data = start_scrapping(row[2],page, browser)
                                if data:
                                    try:
                                        # stored return data from scrapping page store in row
                                        for i,j in data.items():
                                            # print(i)
                                            row.append(j)
                                    except Exception as e:
                                        print(e)

                                rows.append(row)
                                # writing the data rows 
                                csvwriter.writerows(rows)
                                csvfile.close()
      
        except TimeoutError as te:
            print("timeout error",te)
        except Exception as e:
            print(e)

def scrap_with_beautifulsoap(filename,destination):
    try:
        options = webdriver.ChromeOptions()
        # options.add_argument('--ignore-ssl-errors=yes')
        # options.add_argument('--ignore-certificate-errors')
        driver = webdriver.Remote(
        command_executor='http://139.59.12.186:4444/wd/hub',
        desired_capabilities=options.to_capabilities()
        )
        # PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
        # DRIVER_BIN = os.path.join(PROJECT_ROOT, "chromedriver")
        # driver = webdriver.Chrome(DRIVER_BIN)
        # driver.maximize_window() 
        # driver.get(url)
        driver.get("https://www.tripadvisor.in/Search?q=Quinta%20de%20la%20Rosa&searchNearby=false&blockRedirect=true&geo=1&ssrc=h&rf=1")
        xpath2 = '//*[@id="component_3"]/div/div/input[1]'
        WebDriverWait(driver,10).until(EC.presence_of_element_located((By.XPATH,xpath2)))
        
        fields = ['TBOHOTELID','GIATAID','HOTELNAME','ADDRESSLINE1','ADDRESSLINE2','CITYID_NEW','CITYNAME','COUNTRYNAME','COUNTRYCODE','LATITUDE','LONGITUDE','STARRATING','RANK','REMARK','REVIEWS',"LOCATION","CLEANLINESS","SERVICE","VALUE"]
        with open(destination, 'a') as csvfile_1: 
            # create csv file object
            csvwriter1 = csv.writer(csvfile_1)
            # add header in csv file
            csvwriter1.writerow(fields)
            csvfile_1.close()
            with open(filename) as f:
                csvreader = csv.reader(f)
                for row in csvreader:
                    try:
                        driver.switch_to.window(driver.window_handles[0])
                        rows = []
                    
                        if row[2] != "HOTELNAME":
                            search = driver.find_elements(By.TAG_NAME,'input')
                            count = 1
                            for i in search:
                                if i.get_attribute('role')=='searchbox':
                                    if count==2:
                                        i.clear()
                                        i.send_keys(row[2])
                                        i.send_keys(Keys.ENTER)
                                    count +=1
                            # driver.find_element(By.XPATH,xpath2).clear()
                            # driver.find_element(By.XPATH,xpath2).send_keys(row[2])
                            # driver.find_element(By.XPATH,xpath2).send_keys(Keys.ENTER)

                            if row[2]=="Atlantis The Palm Dubai":
                                print("work")
                                #switch to alert box
                                try:
                                    alert = driver.switch_to.alert

                                    if alert:
                                        print(alert.text)
                                        # #sleep for a second
                                        # sleep(5)
                                        #accept the alert
                                        alert.accept()
                                except Exception as e:
                                    pass
                                driver.switch_to.window(driver.window_handles[0])
                    
                            sleep(10)  
                            WebDriverWait(driver,60).until(EC.presence_of_element_located((By.CLASS_NAME,'result-title')))
                            driver.find_element(By.CLASS_NAME,'result-title').click()
                            sleep(10)
                            driver.switch_to.window(driver.window_handles[1])
                            WebDriverWait(driver,60).until(EC.presence_of_element_located((By.ID,'ABOUT_TAB')))
                            about = driver.find_element(By.ID,"ABOUT_TAB")
                            # print(about)

                            filter_text = about.text
                            # filter_text.replace("About","")
                            data = {}
                            filter_splitted_text  = filter_text.split('\n')
                            data['rank'] = filter_splitted_text[1]
                            data['remark'] = filter_splitted_text[2]
                            data['reviews'] = filter_splitted_text[3].replace(" reviews","")


                            classname = driver.find_elements(By.CLASS_NAME,'ui_bubble_rating')
                            # print("classname",classname)
                            rating_name = ["temp","remark_rank","Location","Cleanliness","Service","Value"]
                            for i,j in enumerate(classname):
                                if i <=5 :
                                    rates = j.get_attribute("class")
                                    r  = rates.replace("ui_bubble_rating bubble_","")
                                    data[rating_name[i]] = r
                                    # print(r)

                            data.pop("temp") 
                            data.pop("remark_rank")
                            print(data)
                            
                            if data:
                                try:
                                    # stored return data from scrapping page store in row
                                    for i,j in data.items():
                                        # print(i)
                                        row.append(j)
                                except Exception as e:
                                    print(e)

                                rows.append(row)
                                with open(destination, 'a') as csvfile: 
                                    csvwriter = csv.writer(csvfile)
                                    # writing the data rows 
                                    csvwriter.writerows(rows)
                                    csvfile.close()
                            driver.close()
                            driver.switch_to.window(driver.window_handles[0])
                    except Exception as e :
                        print(e)



        #component_3 > div > div > input.qjfqs._G.B-.z._J.Cj.R0.UXKdo
        # driver.find_element_by_link_text("Get started free").click()
    except Exception as e:
        print("Error : ",e)

