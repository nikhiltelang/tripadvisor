from playwright.sync_api import sync_playwright, TimeoutError
from time import sleep
import csv
from helper import filter_about


def start_scrapping(topic,page, browser):
    # Add topic name in search box
    page.get_by_role("searchbox").last.fill(topic)
    # press Enter key after fill topic
    page.keyboard.press("Enter")
    try:
        page.wait_for_load_state('domcontentloaded')
        # find results
        result = page.locator(".result-title")
        # click on first items in result
        result.first.click()
        # switch 2nd tab
        page2 = page.context.pages[1]
        # fetch all required data in 2nd tab
        about = page2.locator('#ABOUT_TAB').first
        # filter all data 
        rank = filter_about(about)
        # close second tab
        page2.close()
        # return data
        return rank
    except TimeoutError as te:
        print(te)
    except Exception as e:
        print(e)


def run_srap(filename,destination):
    with sync_playwright() as p:
        try:
            # launch chromium browser
            browser = p.chromium.launch(headless=True)
            # open new tab
            page = browser.new_page()
            page.set_default_timeout(0)
            # open tripadvisor website
            page.goto("https://www.tripadvisor.in/Search?q=Quinta%20de%20la%20Rosa&searchNearby=false&blockRedirect=true&geo=1&ssrc=h&rf=1")
            page.wait_for_load_state('domcontentloaded')
            # create list of csv file headers
            fields = ['TBOHOTELID','GIATAID','HOTELNAME','ADDRESSLINE1','ADDRESSLINE2','CITYID_NEW','CITYNAME','COUNTRYNAME','COUNTRYCODE','LATITUDE','LONGITUDE','STARRATING','RANK','REMARK','REVIEWS',"LOCATION","CLEANLINESS","SERVICE","VALUE"]
            # open csv file
            with open(filename) as f:
                with open(destination, 'a') as csvfile_1: 
                    # create csv file object
                    csvwriter1 = csv.writer(csvfile_1)
                    # add header in csv file
                    csvwriter1.writerow(fields)
                    csvfile_1.close()
                    csvreader = csv.reader(f)
                    
                    for i,row in enumerate(csvreader):
                        with open(destination, 'a') as csvfile: 
                            # create csv file object
                            csvwriter = csv.writer(csvfile)
                            rows = []
                            # print(row)
                            if row[2] != "HOTELNAME":
                                # Start scrapping
                                data = start_scrapping(row[2],page, browser)
                                if data:
                                    try:
                                        # stored return data from scrapping page store in row
                                        for i,j in data.items():
                                            # print(i)
                                            row.append(j)
                                    except Exception as e:
                                        print(e)

                                rows.append(row)
                                # writing the data rows 
                                csvwriter.writerows(rows)
                                csvfile.close()



                    
        except TimeoutError as te:
            print("timeout error",te)
        except Exception as e:
            print(e)