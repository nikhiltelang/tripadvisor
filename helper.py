from datetime import datetime
from bs4 import BeautifulSoup  # allows us to search website html for prices
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import os
from urllib.parse import urlparse
from urllib.parse import parse_qs
import requests

class Create_url:
    def __init__(self, hotel_name,city, number_of_adults, number_of_children, number_of_rooms, start_date, end_date):
        self.hotel_name = hotel_name.lower()
        self.city = city
        self.number_of_adults = number_of_adults
        self.number_of_children = number_of_children
        self.number_of_rooms = number_of_rooms
        self.start_date = datetime.strptime(start_date,"%Y-%m-%d")
        self.end_date = datetime.strptime(end_date,"%Y-%m-%d")

    def create_url(self):
        try:
            url = f"https://www.booking.com/searchresults.en-gb.html?label=gen173nr-1FCAEoggI46AdIM1gEaGyIAQGYAQm4AQfIAQ3YAQHoAQH4AQuIAgGoAgO4AorE_p0GwAIB0gIkODBmZjRhZGYtNTUyOS00ZDNjLWFiNTYtNmIzYjFiMDRhYTRi2AIG4AIB&lang=en-gb&sid=7ea5edc3bf39df21bdb0ee8e9a7e4d15&sb=1&sb_lp=1&src=index&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Findex.en-gb.html%3Flabel%3Dgen173nr-1FCAEoggI46AdIM1gEaGyIAQGYAQm4AQfIAQ3YAQHoAQH4AQuIAgGoAgO4AorE_p0GwAIB0gIkODBmZjRhZGYtNTUyOS00ZDNjLWFiNTYtNmIzYjFiMDRhYTRi2AIG4AIB%26sid%3D7ea5edc3bf39df21bdb0ee8e9a7e4d15%26sb_price_type%3Dtotal%26%26&ss={self.city}%2C+India&is_ski_area=&checkin_year={self.start_date.year}&checkin_month={self.start_date.month}&checkin_monthday={self.start_date.day}&checkout_year={self.end_date.year}&checkout_month={self.end_date.month}&checkout_monthday={self.end_date.day}&efdco=1&group_adults={self.number_of_adults}&group_children={self.number_of_children}&no_rooms={self.number_of_rooms}&b_h4u_keep_filters=&from_sf=1&dest_id=4127&dest_type=region&search_pageview_id=82ac2a45871f007c&search_selected=true"
            return url
        except Exception as e:
            print(e)

    def start_scrapping_with_selenium(self,url):
        try:
            options = webdriver.ChromeOptions()
            # options.add_argument('--ignore-ssl-errors=yes')
            # options.add_argument('--ignore-certificate-errors')
            driver = webdriver.Remote(
            command_executor='http://139.59.12.186:4444/wd/hub',
            desired_capabilities=options.to_capabilities()
            )
            # options = webdriver.ChromeOptions()
            # PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
            # DRIVER_BIN = os.path.join(PROJECT_ROOT, "chromedriver")
            # driver = webdriver.Chrome(DRIVER_BIN)
            driver.maximize_window() 
            previes_offset = "&offset=0"
            url = url + previes_offset
            for page in range(0,1000,25):
                url = url.replace(previes_offset,"&offset="+str(page))
                driver.get(url)
                hotels = driver.find_elements(By.CLASS_NAME,'fcab3ed991')
                hotel_name = []
                hotel_price = []
                for hotel in hotels:
                    raw_text = u"\u20B9"
                    if raw_text in hotel.text:
                        hotel_price.append(hotel.text)
                    else:
                        hotel_name.append(hotel.text)

                result = []
                row_dict = {}
                
                hotel_name.pop(0)
                # hotel_name = self.hotel_name.lower()
                for key in hotel_name:
                    for value in hotel_price:
                        key = key.lower()
                        row_dict[key] = value
                        break
                        
                    if self.hotel_name in row_dict.keys():
                        # break
                        return {"hotel_name" :self.hotel_name, "price":row_dict.get(self.hotel_name)}
                    # print(row_dict)
                previes_offset = "&offset="+str(page)
            return {"hotel_name" :self.hotel_name, "price":row_dict.get(self.hotel_name)}

        except Exception as e:
            print(e)

    def scrap_with_beautifulsoap(self,url):
        try:
            print(url)
            user_agent = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15'}
            response = requests.get(url=url, headers=user_agent)
            soap = BeautifulSoup(response.content,features="html.parser")
            hotel_name = self.hotel_name.lower()
            hotel_name = hotel_name.replace(" ","-")
            urls = soap.find_all('div', class_='fcab3ed991')
            for url in urls:
                print(url)
                # if hotel_name in url['href']:
                #     try:
                #         parsed_url = urlparse(url['href'])
                #         captured_value = parse_qs(parsed_url.query)['top_dp'][0]
                #         if captured_value:
                #             data = {"hotel_name":self.hotel_name,"price":captured_value}
                #             return data
                #     except Exception as e:
                #         pass
        except Exception as e:
            print(e)


class hotel_com:
    def __init__(self, hotel_name,city, number_of_adults, number_of_children, number_of_rooms, start_date, end_date):
        self.hotel_name = hotel_name
        self.city = city
        self.number_of_adults = number_of_adults
        self.number_of_children = number_of_children
        self.number_of_rooms = number_of_rooms
        self.start_date = start_date
        self.end_date = end_date
    
    def create_url(self):
        try:
            url = f'https://in.hotels.com/Hotel-Search?adults={self.number_of_adults}&d1={self.start_date}&d2={self.end_date}&destination={self.city}%2C%20India&endDate=2023-02-14&latLong=15.363735886883271%2C74.05397245110255&regionId=6049928&selected=&semdtl=&sort=RECOMMENDED&startDate={self.start_date}&theme=&useRewards=false&userIntent='
            return url
        except Exception as e:
            print(e)

    def start_scrapping_with_selenium(self,url):
        try:
            options = webdriver.ChromeOptions()
            PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
            DRIVER_BIN = os.path.join(PROJECT_ROOT, "chromedriver")
            driver = webdriver.Chrome(DRIVER_BIN)
            driver.maximize_window() 
            driver.get(url)
            hotels = driver.find_elements(By.CLASS_NAME,'uitk-card-link')

            hotel_list = []
            price_list = []
            for hotel in hotels:
                hotel_list.append(hotel.get_attribute('href').split("?")[0].split('/')[-2].replace("-"," "))
                try:
                    parsed_url = urlparse(hotel.get_attribute('href'))
                    captured_value = parse_qs(parsed_url.query)['top_dp'][0]
                    price_list.append(captured_value)
                except Exception as e:
                    print(e)


            result = []
            hotel_list.pop(0)
            for key in hotel_list:
                row_dict = {}
                for value in price_list:
                    row_dict['hotel_name'] = key
                    row_dict['hotel_price'] = value
                    result.append(row_dict)
                    price_list.remove(value)
                    break

            return result
        except Exception as e:
            print(e)

    def scrap_with_beautifulsoap(self,url):
        try:
            print(url)
            user_agent = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15'}
            response = requests.get(url=url, headers=user_agent,timeout=10)
            soap = BeautifulSoup(response.content,features="html.parser")
            hotel_name = self.hotel_name.lower()
            hotel_name = hotel_name.replace(" ","-")
            urls = soap.find_all('a', class_='uitk-card-link')
            for url in urls:
                print(url)
                if hotel_name in url['href']:
                    try:
                        parsed_url = urlparse(url['href'])
                        captured_value = parse_qs(parsed_url.query)['top_dp'][0]
                        if captured_value:
                            # print(captured_value)
                            data = {"hotel_name":self.hotel_name,"price":captured_value}
                            return data
                    except Exception as e:
                        print("Error :",e)
                        pass
        except Exception as e:
            print(e)

class Expedia:
    def __init__(self, hotel_name,city, number_of_adults, number_of_children, number_of_rooms, start_date, end_date):
        self.hotel_name = hotel_name
        self.city = city
        self.number_of_adults = number_of_adults
        self.number_of_children = number_of_children
        self.number_of_rooms = number_of_rooms
        self.start_date = start_date
        self.end_date = end_date
    
    def create_url(self):
        try:
            url = f'https://www.expedia.com/Hotel-Search?adults={self.number_of_adults}&d1={self.start_date}&d2={self.end_date}&destination={self.city}%2C%20India&endDate={self.end_date}&latLong=15.363735886883271%2C74.05397245110255&regionId=6049928&rooms=1&semdtl=&sort=RECOMMENDED&startDate={self.start_date}&theme=&useRewards=false&userIntent='
            return url
        except Exception as e:
            print(e)

    def start_scrapping_with_selenium(self,url):
        try:
            options = webdriver.ChromeOptions()
            PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
            DRIVER_BIN = os.path.join(PROJECT_ROOT, "chromedriver")
            driver = webdriver.Chrome(DRIVER_BIN)
            driver.maximize_window() 
            driver.get(url)
            hotels = driver.find_elements(By.CLASS_NAME,'uitk-card-link')

            hotel_list = []
            price_list = []
            for hotel in hotels:
                hotel_list.append(hotel.get_attribute('href').split("?")[0].split('/')[-1].replace("-"," ").split('.')[0])
                try:
                    parsed_url = urlparse(hotel.get_attribute('href'))
                    captured_value = parse_qs(parsed_url.query)['top_dp'][0]
                    price_list.append(captured_value)
                except Exception as e:
                    print(e)


            result = []
            hotel_list.pop(0)
            for key in hotel_list:
                row_dict = {}
                for value in price_list:
                    row_dict['hotel_name'] = key
                    row_dict['hotel_price'] = value
                    result.append(row_dict)
                    price_list.remove(value)
                    break

            return result
        except Exception as e:
            print(e)

    def scrap_with_beautifulsoap(self,url):
        try:
            user_agent = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15'}
            response = requests.get(url=url, headers=user_agent)
            soap = BeautifulSoup(response.content,features="html.parser")
            hotel_name = self.hotel_name
            hotel_name = hotel_name.replace(" ","-")
            urls = soap.find_all('a', class_='uitk-card-link')
            for url in urls:
                if hotel_name in url['href']:
                    try:
                        parsed_url = urlparse(url['href'])
                        captured_value = parse_qs(parsed_url.query)['top_dp'][0]
                        if captured_value:
                            # print(captured_value)
                            data = {"hotel_name":self.hotel_name,"price":captured_value}
                            return data
                    except Exception as e:
                        pass
        except Exception as e:
            print(e)