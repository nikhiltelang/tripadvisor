
#Download base image ubuntu 16.04
FROM ubuntu

FROM mcr.microsoft.com/playwright/python:v1.21.0-focal

# Update Ubuntu Software repository
RUN apt-get update && apt-get -y install \
    build-essential libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev wget libglib2.0-0 ttf-mscorefonts-installer

# FROM python:3.8
FROM python:3.9
COPY ./app /app


RUN apt-get update
RUN apt-get install -y --no-install-recommends \
        libatlas-base-dev gfortran nginx supervisor 

RUN apt-get install -y libpq-dev libglib2.0-0 libnss3 libnspr4 libatk1.0-0 libatk-bridge2.0-0 libcups2 libglib2.0-0 \
        libdbus-1-3 libatspi2.0-0 libxcomposite1 libxdamage1 libxext6 libxfixes3 libxrandr2 libgbm1 libdrm2 libxkbcommon0 \
        libpango-1.0-0 libcairo2 libasound2


RUN pip3 install uwsgi

RUN mkdir /log

# copy requriment.txt
COPY requirements.txt /requirements.txt

RUN pip install -r /requirements.txt

RUN playwright install

# RUN apt-get update && playwright install-deps

COPY run.py .

CMD [ "python","run.py" ]