﻿# Trip Advisor

In this trip advisor project we are scrapping hotel ranking and stored in csv file


## clone code your system

    git clone https://gitlab.com/nikhiltelang/tripadvisor.git

## Install requirements.txt 

    pip install -r requirements.txt

## Install playwright
    playwright install

## Run Script File

    python main.py

